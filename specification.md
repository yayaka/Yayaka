# Yayaka Protocol

## Abstract
This document specifies Yayaka protocol.

## Attributes from Amorphos Protocol
This document specifies these attributes at very beginning of it
in order to briefly show that this protocol is a service protocols of Amorphos protocol.
Others are specified in below.

<table>
<thead>
<tr><th>Name</th><th>Version</th></tr>
</thead>
<tbody>
<tr><th>yayaka</th><th>0.0.0</th></tr>
</tbody>
</table>

## RFC 2119
The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
"OPTIONAL" in this document are to be interpreted as described in
[RFC 2119](https://tools.ietf.org/html/rfc2119).

## Introduction
The term communication, can be said that it is widely interpreted word.
In here, we extend and limit the meaning of the term;
Every part of a process of passing something expressed by human (or machine) to other humans.
Important points of this meaning are;
the "something" includes the any kind of work,
and the receiver is a human, not a machine.
A good example would be the relation between publishing and reading this document.
They look apart but when we follow this definition, they are both a part of communication.

Also, every "communication" has one or more carriers
such as voices, books, and internet.
Carriers is one of the biggest concern about communication
because they have strong effects to communication.
As an example, when we use voices to communicate, it can not reach to Brazil from Japan
unless using other carriers, and
when a carrier is censored by a bad dictator,
people can not express their feelings freely.
To characterize good carriers, we suggests 5 factors.

1. Handle any kind of work:
People use many types of expression to communicate.
Someone writes a music, other draws a painting, and another uses a source code.
Equally important is new forms of expression are created, one after another, still now.
Good carriers should have capacity and flexibility to handle the various expressions.
2. Reach anywhere:
People prefer spreading their expressions over the world
rather than building a wall around them.
We assume that a good carrier can transport our expression to anywhere we want to send.
3. Everyone is equal:
Inequality of power make us less creative.
If we had authoritarians on a carrier, we might be unjustly sensored or banned from there.
In a good carrier, each one of senders, receivers, and carriers should be equal,
and there is no trouble like that.
4. Social-graph based on preferences:
Receivers can not take all of informations even if they could use their whole life.
Additionally, senders do not want to share their expressions to each living human on Earth
unless they are absolutely confident.
Therefore, receivers need to select sources, and senders need to select destination.
Good carriers should let them do.
5. Easy to maintain:
This factor is easy to explain the reason.
If a carrier is difficult to maintain, it may be easy to end.
We need sustainable networks built by good carriers.

From beginning with thinking about communication, finally, good carrier is illustrated.
Because we desire the good carriers, Yayaka protocol is created.
Yayaka protocol is a communication protocol
which tries to implement a good carrier in the current internet world.

## Related Work

* [IP (Internet Protocol)](https://tools.ietf.org/html/rfc791)
    IP allows us to connect hosts notwithstanding their locations in the World.
    Similarly, Yayaka protocol allows us
    to associate all the communicative subjects,
    and the formed network is abstracted from physical machines and hosts.
* [HVDSGM](https://hakabahitoyo.wordpress.com/2017/06/19/hdvsgm/):
    A conceptual method to implement horizontally and vertically distributed social networking.
    It divides social networking service into 4 individual services
    social-graph service, text service, multimedia service, epidemic service.
    At first, Yayaka protocol is inspired from this.
* [ActivityPub](https://www.w3.org/TR/activitypub/):
    A protocol for decentralized social networking based on ActivityStream.

## Terminology
<table>
<tbody>
<tr>
<td>host</td>
<td>An Amorphos host.</td>
</tr>
<tr>
<td>user</td>
<td>An registered account in an identity service.</td>
</tr>
<tr>
<td>identity</td>
<td>A pair of `identity-host` and `user-id` to identify a user.</td>
</tr>
<tr>
<td>service provider</td>
<td>An individual or organization who provides Yayaka services.</td>
</tr>
<tr>
<td>event</td>
<td>A bunch of information users exchange to communicate.</td>
</tr>
<tr>
<td>shrinked event</td>
<td>A compressed event to transport.</td>
</tr>
<tr>
<td>shrinked event</td>
<td>A compressed event to transport.</td>
</tr>
<tr>
<td>event stream</td>
<td>A list of events which a node have.</td>
</tr>
<tr>
<td>IPL (identity priority list)</td>
<td>An ordered list of identity services to make a user's identity redundant.</td>
</tr>
<tr>
<td>node</td>
<td>A node of social graphs.</td>
</tr>
<tr>
<td>subprotocol</td>
<td>A higher layer protocol of Yayaka protocol.</td>
</tr>
<tr>
<td>user data</td>
<td>data related to a user and can be fetch by an action.</td>
</tr>
</tbody>
</table>

## Basic Concepts

### Small Scale Server Participation
Yayaka protocol allows service providers to keep their servers small.
The funtions of Yayaka are separated into several isolated services,
so service providers do not have to implement all of the functions.

### Painless Migration
Yayaka protocol allows users to easily and seamlessly migrate one service to another service.
If they could not do migration, service providers could have power than users.
For this reason, Yayaka protocol is designed with painless migration.

### Communicate with Sending Events Specified by Subprotocols
Yayaka protocol does not specifies the format of events
and does not work standalone without subprotocol.
This make the specification small and extensible.

## Messaging

### Messaging with Amorphos
Yayaka Protocol uses the Amorphos protocol for messaging.
All Amorphos actions are specified in this document.

### Timeout And Retransmission
If a server could not receive an answer message for a message,
it is treated as timeout action,
and the sender service SHOULD transmit again the message.

### User Data
User data is referred to any sort of data that is related to a user, their posts, activities, ip addresses.
Each yayaka service MUST NOT store user data which can not be acquired by actions
except caches or local legal reasons.
In addition, the caches MUST be invalidated and completely deleted from storages in less than a few days.

## User Authentication
A user can use multiple presentation services,
and identity service have to authorize the services
which may uses different user authentication methods.
In the situation, an identity service issues a token
for authenticating the user and
validates authorization requests from presentation services with the token.

## Distributed Identity

### Identity Priority List
IPL (Identity Priority List) is a list to declare a priority among identities.
A user can have multiple identities by using the list.
The first element of a list has the highest priority and identity priority score of *n*
(*n* is the number of list),
and the last element has the lowest priority and identity priority score of 1.
Each service MUST always use a user identity of the top of the list,
and store a current IPL which is fetched from the top identity.

### Decision of Identity Priority List
A server MUST decide a IPL to use and store
when it receives a message by an identity which is contained in one of stored IPLs and is not the top of the list.
The decision algorithm is shown in below:

1. Fetch IPLs from each identities in the current IPL.
2. Gather IPLs which are the same, and sort unique IPLs by the number of same IPLs (IPL score).
3. Take the IPLs which have the highest IPL score.
4. Sort the taken IPLs by the sum of identity scores listed in each IPL.
5. The first IPL is the next current IPL.

### Permissions
For security purposes, a user can permit hosts, services, and users to do particular operations.
Each user have a list of permissions, and its items have two properties, a subject and an operation.
A subject have 3 optional properties, host, service, and user, and missed properties are treated as wildcards.
Also, each user has a list of required permission types.
If a host does not implement one or more permission types which a user required,
the host MUST not create a message named by the user,
and all other host MUST ignore the named messages from the host.


## Amorphos Services

### Overview
Yayaka protocol has 4 Amorphos services, which are specified below.
They MUST implement all of Amorphos actions listed below,
and details of those actions are specified in the next section.

### Identity Service
An identity service provides following functions:

* Manage users' identities
* Manage and host an identity priority list
* Store and host users' attributes
* Authenticate users
* Manage permissions

#### Actions
An identity service MUST implement handers of the actions listed below.

* **create-user**
* **update-identity-priority-list**
* **get-identity-priority-list**
* **update-user-attribute**
* **get-user-attributes**
* **get-user-attribute**
* **get-authentication-token**
* **authenticate-user**
* **update-permissions**
* **get-permissions**
* **update-required-permission-types**
* **get-required-permission-types**
* **delete-user-data**
* **get-associated-services**

### Repository Service
A repository service provides following functions:

* Persist and host events
* Broadcast events to social-graphs
* Update events and broadcast
* Delete events and announce

### Actions
A repository service MUST implement handers of the actions listed below.

* **create-event**
* **broadcast-event**
* **get-event**
* **get-event-ids**
* **update-event**
* **delete-event**
* **delete-user-data**
* **get-associated-services**

### Social-graph Service
A social-graph service provides following functions:

* Manage nodes
* Store and host stream of shrinked events
* Follow nodes
* Broadcast shrinked events to nodes
* Update shrinked events
* Delete shrinked events

### Actions
A social-graph service MUST implement handers of the actions listed below.

* **create-node**
* **delete-node**
* **get-nodes**
* **connect-nodes**
* **disconnect-nodes**
* **get-node-connections**
* **get-event-stream**
* **subscribe-stream**
* **unsubscribe-stream**
* **extend-stream-subscription**
* **push-event**
* **update-event**
* **delete-event**
* **delete-user-data**
* **get-associated-services**

### Presentation Service
A presentation service provides following functions:

* UI
* API
* Push notifications

## Actions
A presentation service MUST implement handers of the actions listed below.

* **push-event**
* **update-event**
* **delete-event**
* **delete-user-data**

## Actions
### Answer Message
An answer message for each action has the following properties in its payload.

* *status* string REQUIRED  
  One of the following statuses
  * *ok* The action was handled properly.
  * *failed* The action was failed.
  * *already-handled* The given action ID was already handled.
  * *wrong* The action is wrong.
  * *not-found* The given entity does not exists.
  * *expired* The given entity was expired.
  * *not-permitted* The operation is not permitted by the user.
  * *error* The service can not answer properly by their technically problems.
* *body* object OPTIONAL

```json
{
  "id": "abcdefghij",
  "reply-to": "0123456789",
  "host": "host1.example.com",
  "protocol": "yayaka",
  "service": "presentation",
  "action": "update-user-name",
  "payload": {
    "status": "ok"
  },
  "sender-host": "host2.example.com",
  "sender-protocol": "yayaka",
  "sender-service": "identity"
}
```

### Property Types
In the action specifications, the following types are used.

<table>
<thead>
<tr><th>Name</th><th>Description</th></tr>
</thead>
<tbody>
<tr><td>string</td><td>a JSON string</td></tr>
<tr><td>host</td><td>a hostname, JSON string</td></tr>
<tr><td>service</td><td>one of `"identity"`, `"repository"`, `"social-graph"`, and `"presentation"`.</td></tr>
<tr><td>protocol-identifier</td><td>an identifier of a subprotocol, JSON string</td></tr>
<tr><td>object</td><td>a JSON object</td></tr>
<tr><td>array</td><td>a JSON array</td></tr>
<tr><td>integer</td><td>a JSON integer</td></tr>
<tr><td>identity</td><td>a JSON object described in below.</td></tr>
<tr><td>node</td><td>a JSON object described in below.</td></tr>
<tr><td>permission-subject</td><td>an JSON object described in below.</td></tr>
<tr><td>operation</td><td>an JSON object described in below.</td></tr>

* identity
  An object which has the following properties:

  * *identity-host* host REQUIRED
  * *user-id* string REQUIRED

  ```json
  {
    "identity-host": "example.com",
    "user-id": "example-user"
  }
  ```

* node
  An object which has the following properties:

  * *social-graph-host* host REQUIRED
  * *node-id* string REQUIRED

  ```json
  {
    "social-graph-host": "example.com",
    "node-id": "example-node"
  }
  ```

* permission-subject
  An object which has the following properties:

  * *host* host OPTIONAL
  * *service* service OPTIONAL
  * *user* identity OPTIONAL

  ```json
  {
    "host": "yayaka-essential-permissions",
    "service": "update-attribute.icon",
    "user": {
      "identity-host": "example.com",
      "user-id": "example-user"
    }
  }
  ```

* operation
  An object which has the following properties:

  * *protocol* protocol-identifier REQUIRED
  * *type* string REQUIRED
  * *key* string REQUIRED

  ```json
  {
    "protocol": "yayaka-essential-permissions",
    "type": "update-attribute",
    "key": "user-name.user-name-registry-host"
  }
  ```

### Unexpected Actions
When A Yayaka service got unexpected actions,
unknown or having wrong format,
the service SHOULD return an answer with the status *wrong*.

### **create-user**
Create a user identity.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload is empty.
* Answer has a *body* with the following properties:
* *user-id* string REQUIRED
    A created *user-id*

### **update-identity-priority-list**
Update a identity priority list.
The list MUST contains the destination host.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user-id* string REQUIRED
  * *identity-priority-list* array REQUIRED
    An array of identity objects.
* Answer has an empty *body*

### **get-identity-priority-list**
Get a identity priority list.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user-id* string REQUIRED
* Answer has a *body* with the following properties:
  * *identity-priority-list* array REQUIRED
    An array of objects which have the following properties:
    * *host* host REQUIRED
    * *user-id* string REQUIRED

### **get-user-attributes**
Fetch user's attributes.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user-id* string
* Answer has *body* with the following properties:
  * *attributes* array REQUIRED
    An array of objects which have the following properties:
    * *protocol* protocol-identifier REQUIRED
    * *key* string REQUIRED
    * *value* object REQUIRED
    * *sender-host* string

### **update-user-attribute**
Update a user attribute.
Delete the attribute if a value is *null* and the attribute is exists.
Insert the attribute if a value is not *null* and the attribute is not exists.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user-id* string REQUIRED
  * *protocol* protocol-identifier REQUIRED
      a subprotocol
  * *key* string REQUIRED
  * *value* object or null REQUIRED
* Answer has an empty *body* if successfully updated,
or a *body* which has properties specifed in the subprotocol if failed.

### **get-user-attribute**
Get a user's attributes.

* Destination MUST be an identity service.
* Payload has the following properties:
  * *user-id* string
* Answer has *body* with the following properties:
  * *user-attributes* array REQUIRED
    An array of objects which have the following properties:
    * *protocol* protocol-identifier REQUIRED
    * *key* string REQUIRED
    * *value* object REQUIRED
    * *sender-host* string REQUIRED

### **get-authentication-token**
Fetch a token for authenticating a user.
The destination identity service MUST issue a new token
for each time of this action.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user-id* string REQUIRED
  * *presentation-host* string REQUIRED
* Answer has *body* with the following properties:
  * *token* string REQUIRED
  * *expires* integer REQUIRED
    A number of seconds from 1970-01-01T00:00:00Z without applying leap seconds.

### **authenticate-user**
Authenticate a user.
The destination identity MUST authenticate the user
only when the token is matched with the latest token issued for
the sender presentation service by the user.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user-id* string REQUIRED
  * *token* string REQUIRED
* Answer has an empty *body*.

### **update-permissions**
Update permissions for a user.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user-id* string REQUIRED
  * *permissions* array REQUIRED
      An array of objects which have the following properties:
    * *subject* permission-subject REQUIRED
    * *permission* permission REQUIRED
* Answer has an empty *body*.

### **get-permissions**
Fetch permissions of a user.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user-id* string REQUIRED
* Answer has a *body* which has the following properties:
  * *permissions* array REQUIRED
      An array of objects which have the following properties:
    * *subject* permission-subject REQUIRED
    * *permission* permission REQUIRED

### **update-required-permission-types**
Update a list of permission types required by a user.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user-id* string REQUIRED
  * *permission-types* array REQUIRED
      An array of objects which have the following properties:
    * *protocol* protocol-identifier REQUIRED
    * *type* string REQUIRED
* Answer has an empty *body*.

### **get-required-permission-types**
Fetch a list of permission types required by a user.

* Destination MUST be an identity service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user-id* string REQUIRED
* Answer has a *body* which has the following properties:
  * *permission-types* array REQUIRED
      An array of objects which have the following properties:
    * *protocol* protocol-identifier REQUIRED
    * *type* string REQUIRED

### **create-event**
Create an event.

* Destination MUST be a repository service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user* identity REQUIRED
  * *protocol* protocol-identifier REQUIRED
  * *type* string REQUIRED
  * *body* object REQUIRED
* Answer has *body* with the following properties:
  * *event-id* string REQUIRED

### **broadcast-event**
Broadcast an event.

* Destination MUST be a repository service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *event-id* string REQUIRED
  * *nodes* array REQUIRED
    An array of objects which have the following properties:
    * *node* node REQUIRED
* Answer has an empty *body*.

### **get-event**
Get an event.

* Destination MUST be a repository service.
* Payload has the following properties:
  * *event-id* string REQUIRED
* Answer has *body* with the following properties:
  * *user* identity REQUIRED
  * *protocol* protocol-identifier REQUIRED
  * *type* string REQUIRED
  * *body* object REQUIRED
  * *sender-host* string REQUIRED
    A presentation host which called *create-event*.
  * *created-at* datetime REQUIRED

### **get-event-ids**
Get a list of event IDs which are created by the user.

* Destination MUST be a repository service.
* Payload has the following properties:
  * *user* identity REQUIRED
* Answer has *body* with the following properties:
  * *events* array REQUIRED
      An array of objects which have the following properties:
    * *event-id* string REQUIRED

### **update-event**
Update an event.
Destination services MUST send all services to which the event was broadcasted.

1. When the destination service is a repository service.
* Destination MUST be a repository service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *event-id* string REQUIRED
  * *body* object REQUIRED
* Answer has an empty *body*.

2. When the destination service is a social-graph service.
* Destination MUST be a social-graph service.
* Sender MUST be the repository service which owns the event.
* Payload has the following properties:
  * *event-id* string REQUIRED
  * *body* object REQUIRED
* Answer has an empty *body*.

3. When the destination service is a presentation service.
* Destination MUST be a presentation service.
* Sender MUST be a social-graph service which is the source of the event.
* Payload has the following properties:
  * *event-id* string REQUIRED
  * *body* object REQUIRED
* Answer has an empty *body*.

### **delete-event**
Delete an event.
Destination services MUST delete the event completely from their strage or something
and MUST send all services to which the event was broadcasted.

1. When the destination service is a repository service.
* Destination MUST be a repository service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *event-id* string REQUIRED
* Answer has an empty *body*.

2. When the destination service is a social-graph service.
* Destination MUST be a social-graph service.
* Sender MUST be the repository service which owns the event.
* Payload has the following properties:
  * *event-id* string REQUIRED
* Answer has an empty *body*.

3. When the destination service is a presentation service.
* Destination MUST be a presentation service.
* Sender MUST be a social-graph service which is the source of the event.
* Payload has the following properties:
  * *event-id* string REQUIRED
* Answer has an empty *body*.

### **create-node**
Create a node

* Destination MUST be a social graph service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user* identity REQUIRED
* Answer has *body* with the following properties:
  * *node-id* string REQUIRED

### **delete-node**
Delete a node

* Destination MUST be a social graph service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *node-id* string REQUIRED
* Answer has an empty *body*.

### **get-nodes**
Get nodes which is create by the user.

* Destination MUST be a social graph service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user* identity REQUIRED
* Answer has the following properties:
  * *nodes* object REQUIRED
      An array of objects which have the following properties:
    * *node-id* string REQUIRED

### **connect-nodes**
Connect between nodes.

* Destination MUST be a social graph service.
* Sender MUST be a presentation service
    by one owner of the two nodes.
* Payload has the following properties:
  * *source-node* node REQUIRED
  * *destination-node* node REQUIRED
* Answer has an empty *body*.

### **disconnect-nodes**
Disconnect between nodes.

* Destination MUST be a social graph service.
* Sender MUST be a presentation service
    by one owner of the two nodes.
* Payload has the following properties:
  * *source-node* node REQUIRED
  * *destination-node* node REQUIRED
* Answer has an empty *body*.

### **get-node-connecions**
Get a connection list of a node.

* Destination MUST be a social graph service.
* Sender MUST be a presentation service.
* Payload has the following property:
  * *node-id* string REQUIRED
* Answer has a *body* which has the following properties:
  * *connections* array REQUIRED
      An array of objects which have the following properties:
    * *source-node* node REQUIRED
    * *destination-node* node REQUIRED

### **get-event-stream**
Fetch an event stream.
The desitination social graph service returns
list of recent events which are item .

* Destination MUST be a social graph service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user* identity REQUIRED
  * *limit* integer OPTIONAL
  * *skip-before-item-id* string OPTIONAL
  * *skip-after-item-id* string OPTIONAL
* Answer has *body* with the following properties:
  * *items* array REQUIRED
    An array of objects which have following properties:
    * *item-id* string REQUIRED
        A unique ID of the item of the stream
    * *created-at* datetime REQUIRED
    * *shrinked-event* object REQUIRED
        An object which has the following properties:
      * *repository-host* host REQUIRED
      * *event-id* string REQUIRED
      * *user* identity REQUIRED
      * *protocol* protocol-identifier REQUIRED
      * *type* string REQUIRED
      * *body* object REQUIRED
      * *sender-host* string REQUIRED
        A presentation host which called *create-event*.
      * *created-at* datetime REQUIRED

### **subscribe-stream**
Request broadcast of an event stream.
The destination social graph service sends *push-event* to the sender
when it receives *push-event* from other services.

* Destination MUST be a social graph service.
* Sender MUST be a presentation service.
* Payload has the following properties:
  * *user* identity REQUIRED
  * *limit* integer REQUIRED
  * *last-item-id OPTIONAL
      If this property is given,
      the destination service can ignore the item and items which are older than it.
* Answer has *body* with the following properties:
  * *subscription-id* string REQUIRED
  * *expires* integer  
    A number of seconds from 1970-01-01T00:00:00Z without applying leap seconds.

### **unsubscribe-stream**
Cancel broadcast a event stream.

* Destination MUST be a social graph service.
* Sender MUST be the subscriber presentation service.
* Payload has the following properties:
  * *subscription-id*
* Answer has an empty *body*.

### **extend-stream-subscription**
Extend a subscripion of a event stream.

* Destination MUST be a social graph service.
* Sender MUST be the subscriber presentation service.
* Payload has the following properties:
  * *subscription-id*
* Answer has *body* with the following properties:
  * *expires* integer  
    A number of seconds from 1970-01-01T00:00:00Z without applying leap seconds.

### **push-event**
Push a new event.

1. When the destination service is a social graph service.
The destination service sends *push-event* to
all nodes following the destination node,
except nodes which is contained in the *route* of the payload.
and all presentation service subscribing the event stream.
Also, the destination MUST ignore actions which have the destination node in the *route*

* Destination MUST be a social graph service.
* Sender MUST be a repository service or a social graph service.
* Payload has the following properties:
  * *source-node* node REQUIRED
  * *destination-node* node REQUIRED
  * *item-id* string REQUIRED
      A unique ID of the item of the event stream
  * *created-at* datetime REQUIRED
    * *shrinked-event* object REQUIRED
        An object which has the following properties:
      * *repository-host* host REQUIRED
      * *event-id* string REQUIRED
      * *user* identity REQUIRED
      * *protocol* protocol-identifier REQUIRED
      * *type* string REQUIRED
      * *body* object REQUIRED
      * *sender-host* string REQUIRED
        A presentation host which called *create-event*.
      * *created-at* datetime REQUIRED
    * *route* array REQUIRED
        An list of all nodes which pushes the item except the destination node .
        An array of objects which have the following properties:
      * *node* node REQUIRED
* Answer has an empty *body*.

2. when the desitination service is a presentation service.

* Destination MUST be a resentation service.
* Sender MUST be a social graph service.
* Payload has the following properties:
  * *subscription-id* string REQUIRED
  * *item-id* string REQUIRED
      A unique ID of the item of the event stream
  * *created-at* datetime REQUIRED
    * *shrinked-event* object REQUIRED
        An object which has the following properties:
      * *repository-host* host REQUIRED
      * *event-id* string REQUIRED
      * *user* identity REQUIRED
      * *protocol* protocol-identifier REQUIRED
      * *type* string REQUIRED
      * *body* object REQUIRED
      * *sender-host* string REQUIRED
        A presentation host which called *create-event*.
      * *created-at* datetime REQUIRED
    * *route* array REQUIRED
        An list of all nodes which pushes the item except the destination node .
        An array of objects which have the following properties:
      * *node* node REQUIRED
* Answer has an empty *body*.

### **delete-user-data**
Delete a whole user data.
A destination service MUST delete the data of the user completely from its starage.

* Sender service MUST be a presentation service.
* Payload has the following properties:
  * *user* identity REQUIRED
* Answer has an empty *body*.

### **get-associated-services**
Get all services to which the desitination service send user data.

* Payload has the following properties:
  * *user* identity REQUIRED
* Answer has an *body* which has the following properties:
  * *services* array REQUIRED
      An array of objects which have the following properties:
    * *host* host REQUIRED
    * *service* service REQUIRED


## Subprotocol
### Overview
Yayaka Protocol has subprotocols.
A yayaka instance SHOULD specify implemented subprotocol in the protocol parameters.
A name of a subprotocol MUST be a lowercase [a-z] string joined by '-'.
A specification of a subprotcol contains the following descriptions:

* REQUIRED its name
* REQUIRED its identifier
* REQUIRED its version
* OPTIONAL list of user attribute properties
* OPTIONAL list of event types and their shrinked format
* OPTIONAL list of permission types
* REQUIRED parameters

## Parameters
The service protocol parameters has the following property.

* **subprotocols** array
  An array of objects which have following properties
  Same protocols with different versions are allowed.
  * *identifier* string REQUIRED
      The identifier of the subprotocol
  * *version* semver REQUIRED
      The version of the subprotocol
  * *user-attribute-properties* array REQUIRED
      A list of the implemented user attributes.
      An array of objects which have the following properties:
    * *key* string REQUIRED
  * *event-types* array REQUIRED
      A list of the implemented event types.
      An array of objects which have the following properties:
    * *key* string REQUIRED
  * *permission-types* array REQUIRED
      A list of the implemented permission types.
      An array of objects which have the following properties:
    * *type* string REQUIRED
  * *parameters* object OPTIONAL
      The parameters for the subprotocol

## Acknowledgements
We thank Michael for the revision.
