# Yayaka Protocol

Yayaka Protocol is a protocol for highly distributed social networking.

## Purposes

* Handle any kind of work
* Reach anywhere
* Everyone Is Equal
* Social-graph Based on Preferences
* Easy to Maintain

## Basic Concepts
* Small scale server participation
* Painless migration
* Communicate with exchanging events specified by subprotocols

## Specification

See [Yayaka Protocol Specification](specification.md).

## Implementations

There is no implementations yet.
[yayaka-middleware](https://gitlab.com/yayaka/yayaka-middleware) will be the first one.

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
